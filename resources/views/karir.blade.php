@extends('temp.template')
@section('content')

<section class="herokarir" data-aos="fade-up">
    <div class="herokarir-text">
        <h1>Selamat Datang <span>Rekrutmen</span> PRAMA LOGISTIC</h1>
    </div>
</section>
<section class="lowongan">
    <div class="container">
        <h1 style="color: #FE3F00; text-align: center; margin-bottom:3%;">LOWONGAN</h1>
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6 col-sm-12 col-12 d-flex align-items-center mb_30ml" data-aos="zoom-out" data-aos-delay="100">
                <div class="tulisan-lowongan">
                    <h2>Staff Accounting</h2>
                </div>
                <div class="btn-lwn-pos">
                    <a href="#1"><button onclick="syarat(event, '1')" class="tablinks btn-lwn" type="button">Selengkapnya</button></a>
                </div>
                <img class="lwn-image" src="{{ url ('assets/img/akuntansi.png') }}">
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-12 d-flex align-items-center mb_30ml" data-aos="zoom-out" data-aos-delay="100">
                <div class="tulisan-lowongan">
                    <h2>Marketing Manager</h2>
                </div>
                <div class="btn-lwn-pos">
                    <a href="#2"><button onclick="syarat(event, '2')" class="tablinks btn-lwn" type="button">Selengkapnya</button></a>
                </div>
                <img class="lwn-image" src="{{ url ('assets/img/manajermarketing.png') }}">
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-12 d-flex align-items-center mb_30ml" data-aos="zoom-out" data-aos-delay="100">
                <div class="tulisan-lowongan">
                    <h2>Mechanical Engineer</h2>
                </div>
                <div class="btn-lwn-pos">
                    <a href="#3"><button onclick="syarat(event, '3')" class="tablinks btn-lwn" type="button">Selengkapnya</button></a>
                </div>
                <img class="lwn-image" src="{{ url ('assets/img/insinyurmekanik.png') }}">
            </div>
        </div>
    </div>
</section>
<section id="1" class="persyaratan">
    <div class="container">
        <h1 style="color: #FE3F00; text-align: center; margin-bottom:3%;">PERSYARATAN</h1>
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-6 col-sm-12 col-12 mb_30m">
                <img class="lwn-image" src="{{ url ('assets/img/account.png') }}">
                <div class="row justify-content-center">
                    <div class="col-lg-5 col-md-6 col-sm-12 col-12 mb_30m text-center">
                        <a href="{{url('/karir/daftar')}}"><button class="btn-lwn-daftar" type="button">Daftar</button></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12 align-items-center text-justify mb_30">
                <h2>STAFF ACCOUNTING</h2>
                <ul>
                    <li>Pendidikan Min S1 Akuntansi</li>
                    <li>Pengalaman Min 1 Tahun di bidangnya (Fresh Graduate Diperkenankan)</li>
                    <li>Diutamakan yang berdomisili di Ponorogo atau Tuban</li>
                </ul>
                <h5>Deskripsi Pekerjaan</h5>
                <ul>
                    <li>Melakukan input dan pencatatan laporan keuangan PRAMA Logistic</li>
                    <li>Mengawasi dan mencatat setiap transaksi keuangan PRAMA Logistic</li>
                    <li>Berkoordinasi dengan Manajer Accounting terkait kegiatan keuangan yang ada di PRAMA Logistic</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section id="2" class="persyaratan">
    <div class="container">
        <h1 style="color: #FE3F00; text-align: center; margin-bottom:3%;">PERSYARATAN</h1>
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-6 col-sm-12 col-12 mb_30m">
                <img class="lwn-image" src="{{ url ('assets/img/marketing.png') }}">
                <div class="row justify-content-center">
                    <div class="col-lg-5 col-md-6 col-sm-12 col-12 mb_30m text-center">
                        <a href="{{url('/karir/daftar')}}"><button class="btn-lwn-daftar" type="button">Daftar</button></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12 align-items-center text-justify mb_30">
                <h2>Marketing Manager</h2>
                <ul>
                    <li>Pendidikan Min S1 Marketing/Manajemen</li>
                    <li>Memiliki Pengalaman min 3 Tahun di bidangnya</li>
                    <li>Diutamakan yang berdomisili di Ponorogo atau Tuban</li>
                </ul>
                <h5>Deskripsi Pekerjaan</h5>
                <ul>
                    <li>Mengatur kegiatan pemasaran dan promosi PRAMA Logistic</li>
                    <li>Melakukan Koordinasi dengan para staff, mengenai kegiatan pemasaran PRAMA Logistic</li>
                    <li>Mengatur dan merancang strategi pemasaran PRAMA Logistic</li>
                    <li>Mengkreasi berbagai konten terkait dengan kegiatan pemasaran PRAMA Logistic</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section id="3" class="persyaratan">
    <div class="container">
        <h1 style="color: #FE3F00; text-align: center; margin-bottom:3%;">PERSYARATAN</h1>
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-6 col-sm-12 col-12 mb_30m">
                <img class="lwn-image" src="{{ url ('assets/img/engineer.png') }}">
                <div class="row justify-content-center">
                    <div class="col-lg-5 col-md-6 col-sm-12 col-12 mb_30m text-center">
                        <a href="{{url('/karir/daftar')}}"><button class="btn-lwn-daftar" type="button">Daftar</button></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12 align-items-center text-justify mb_30">
                <h2>MECHANICAL ENGINEER</h2>
                <ul>
                    <li>Pendidikan Min S1 Teknik Mesin</li>
                    <li>Pengalaman Min 1 Tahun di bidangnya (Fresh Graduate Diperkenankan)</li>
                    <li>Diutamakan yang berdomisili di Ponorogo atau Tuban</li>
                </ul>
                <h5>Deskripsi Pekerjaan</h5>
                <ul>
                    <li>Melakukan kegiatan maintenence terhadap armada PRAMA Logistic secara berkala</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<script>
    function syarat(evt, nomer) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("persyaratan");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }
      document.getElementById(nomer).style.display = "block";
      evt.currentTarget.className += " active";
    }
    </script>

@endsection