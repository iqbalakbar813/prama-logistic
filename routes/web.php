<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','BerandaController@index');
Route::get('/tentang_kami','TentangController@index');
Route::get('/karir','KarirController@index');
Route::get('/berita','BeritaController@index');
Route::get('/pesan','PesanController@index');
Route::get('/karir/daftar','KarirController@daftar');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
